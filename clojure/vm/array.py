from clojure.vm.object import Object, type_ids
from clojure.vm.primitives import nil


class Array(Object):
    type_id = type_ids.next()

    def __init__(self, data, meta = nil):
        self.data = data
        self.meta = meta

    def type(self):
        return self.type_id

    def aclone(self):
        return Array(self.data[:], self.meta)

    def aget(self, idx):
        assert isinstance(idx, int)
        return self.data[idx]

    def aset(self, idx, val):
        assert isinstance(idx, int)
        assert isinstance(val, Object)
        self.data[idx] = val

    def alength(self):
        return len(self.data)

