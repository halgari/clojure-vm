from clojure.vm.object import Object, type_ids
from clojure.vm.string import String, intern as intern_string, intern_from_str
from clojure.vm.primitives import nil



class Symbol(Object):
    type_id = type_ids.next()

    def type(self):
        return self.type_id

    def __init__(self, *args):
        if len(args) == 1:
            assert isinstance(args[0], String)
            self.ns = nil
            self.name = intern_string(args[0])
            self.meta = nil
        if len(args) == 2:
            assert isinstance(args[0], String)
            assert isinstance(args[1], String)
            self.ns = intern_string(args[0])
            self.name = intern_string(args[1])
            self.meta = nil
        if len(args) == 3:
            assert isinstance(args[0], String) or args[0] is nil
            assert isinstance(args[1], String)
            assert isinstance(args[2], Object)
            self.ns = intern_string(args[0]) if args[0] is not nil else nil
            self.name = intern_string(args[1])
            self.meta = args[2]

        if self.ns is not nil:
            self.str = intern_from_str(self.ns.value + "/" + self.name.value)
        else:
            self.str = self.name

        assert 0 < len(args) < 4, "Arity Exeception"


def from_str(*args):
    if len(args) == 1:
        assert isinstance(args[0], str)
        return Symbol(String(args[0]))
    if len(args) == 2:
        assert isinstance(args[0], str)
        assert isinstance(args[1], str)
        return Symbol(String(args[0]), String(args[1]))

    assert 0 < len(args) < 3, "Arity Exception"