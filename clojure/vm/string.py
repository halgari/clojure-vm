from clojure.vm.object import Object, type_ids


class String(Object):
    type_id = type_ids.next()

    def __init__(self, value):
        self.value = value

    def type(self):
        return self.type_id

    def to_string(self):
        return self.value

    def concat(self, other):
        assert isinstance(other, String)
        return String(self.value + other.value)


def wrap(value):
    return String(value)


class InternedStringCache(object):
    def __init__(self):
        self.cache = {}

    ## TODO - Lock
    def intern(self, value):
        assert isinstance(value, String)
        unwrapped = value.to_string()

        if unwrapped in self.cache:
            return self.cache[unwrapped]

        self.cache[unwrapped] = value
        return value

intern_cache = InternedStringCache()


def intern(value):
    assert isinstance(value, String)
    return intern_cache.intern(value)

def intern_from_str(value):
    assert isinstance(value, str)
    return intern(String(value))