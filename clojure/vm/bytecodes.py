from clojure.vm.object import Object, type_ids, Invokable
from rpython.rlib.jit import unroll_safe, elidable


BYTECODES = [
    'NO_OP',
    'POP',         ## Pops TOS
    'PUSH_CONST',  ## Pushes the const at idx arg onto the stack
    'PUSH_VAR',    ## Pushes the var at the idx arg onto the stack
    'DEREF_VAR',   ## Pops a var from the stack and pushes the deref of that var
    'INIT_VAR',
    'INVOKE',      ## fn is at TOS, args are below that
    'APPLY',       ## like invoke, but the last arg is considered to be a seq
    'INIT_FIELD',  ## sets the value of a field. can be mutable or imutable field
    'GET_FIELD',   ## gets the value of a field
    'SET_FIELD',   ## sets the value of a mutable field

    'PUSH_LOCAL',
    'STORE_LOCAL',
    'PUSH_ARG',

    'JMP',
    'COND_JMP',

    'EQ',
    'LT',
    'GT',
    'LTE',
    'GTE',
    'NEQ',
    'ADD',
    'SUB',
    'MUL',
    'DIV',
    'MOD',
    'BIT_SHIFT_RIGHT',
    'BIT_SHIFT_LEFT',
    'BIT_AND',
    'BIT_OR',
    'BIT_XOR',
    'BIT_NOT',
    'BIT_COUNT',
    'RETURN',


]



for x in range(len(BYTECODES)):
    globals()[BYTECODES[x]] = x

HAS_ARG = [
    PUSH_CONST,
    PUSH_VAR,
    INVOKE,
    APPLY,
    PUSH_LOCAL,
    STORE_LOCAL,
    JMP,
    COND_JMP
]



class Code(Invokable):
    type_id = type_ids.next()
    _immutable_fields_ = ["bytecodes", "vars", "conts", "stacksize", "locals"]

    def type(self):
        return self.type_id

    def __init__(self, name, arity, bytecodes, vars, consts, stacksize, locals):
        self.name = name
        self.arity = arity
        self.bytecodes = bytecodes
        self.vars = vars
        self.consts = consts
        self.stacksize = stacksize
        self.locals = locals

    def call(self, args, argc):
        from clojure.vm.interpreter import interpret, Frame
        f = Frame(self)
        args.unpack(f)

        return interpret(self, f)

class Fn(Invokable):
    type_id = type_ids.next()
    _immutable_fields_ = ["fn_name", "arities"]

    def type(self):
        return self.type_id

    def __init__(self, fn_name, arities):
        self.fn_name = fn_name
        self.arities = arities

    @elidable
    def get_arity(self, argc):
        return self.arities[argc]

    def call(self, args, argc):
        fn = self.get_arity(argc)
        return fn.call(args, argc)




