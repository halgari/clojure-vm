from clojure.vm.object import PolymorphicFn, Invokable
from clojure.vm.namespace import export_var


_first = export_var("clojure.core", "_first")(PolymorphicFn())

@export("clojure.core", "first")
class first
