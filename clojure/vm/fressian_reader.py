from clojure.vm.namespace import intern_var_from_str
from clojure.vm.primitives import Integer
from clojure.vm.bytecodes import Code, Fn
from clojure.vm.string import String
from clojure.vm.array import Array


INT_PACKED_2_ZERO = 0x50
INT_PACKED_3_ZERO = 0x68
INT_PACKED_4_ZERO = 0x72
INT_PACKED_5_ZERO = 0x76

STRING_PACKED_LENGTH_START = 0xDA
STRING_PACKED_LENGTH_END = 0xE2

BYTES_PACKED_LENGTH_START = 0xD0

PRIORITY_CACHE_PACKED_START = 0x80
PRIORITY_CACHE_PACKED_END = 0xA0

STRUCT_CACHE_PACKED_START = 0xA0
STRING = 0xE3


class Reader(object):
    def __init__(self, data):
        self.data = data
        self.idx = 0

    def next_byte(self):
        b = self.data[self.idx]
        self.idx += 1
        return ord(b)

def read_object_array(rdr, data):
    ret_data = []
    count = read_int(rdr)
    for x in range(count):
        data = rdr.next_byte()
        ret_data.append(read_item(rdr, data))

    return Array(ret_data)

def read_int(rdr):
    code = rdr.next_byte()
    if code == 0xFF:
        return code
    if 0x00 <= code <= 0x3F:
        return code & 0xff

    if 0x60 <= code <= 0x6F:
        return (code - INT_PACKED_3_ZERO) << 16 | read_raw_int_16(rdr)

    assert False, "Implement more of readInt"

def read_count(rdr):
    return read_int(rdr)


def read_byte_array(rdr, data):
    count = read_count(rdr)
    data = []
    for x in range(count):
        data.append(chr(rdr.next_byte()))
    return String("".join(data))

def read_raw_int_16(rdr):
    return (rdr.next_byte() << 8) + rdr.next_byte()

def read_raw_int_24(rdr):
    return (rdr.next_byte() << 16) + (rdr.next_byte() << 8) + rdr.next_byte()

def read_raw_int_32(rdr):
    return (read_raw_int_24(rdr) << 8) + rdr.next_byte()

def read_raw_int_40(rdr):
    return read_raw_int_32(rdr) << 8 + rdr.next_byte()

def internal_read_string(rdr, length):
    data = []
    for x in range(length):
        data.append(chr(rdr.next_byte()))
    return "".join(data)

def internal_read_bytes(rdr, length):
    data = []
    for x in range(length):
        data.append(chr(rdr.next_byte()))
    return "".join(data)


def read_item(rdr, data):
    if data == 0xb5:  # Object Array
        return read_object_array(rdr, data)

    if data == 0xD9:
        return read_byte_array(rdr, data)

    if data == 0xFF:
        return Integer(-1)

    if 0x00 <= data <= 0x3F:
        return Integer(data & 0xFF)

    if 0x40 <= data <= 0x5F:
        return Integer(((data - INT_PACKED_2_ZERO) << 8) | rdr.next_byte())

    if 0x06 <= data <= 0x6F:
        return Integer(((data - INT_PACKED_3_ZERO) << 16) | read_raw_int_16(rdr))

    if 0x70 <= data <= 0x73:
        return Integer(((data - INT_PACKED_4_ZERO) << 24) | read_raw_int_24(rdr))

    if 0x74 <= data <= 0x77:
        return Integer(((data - INT_PACKED_5_ZERO) << 40) | read_raw_int_40(rdr))

    if data == 0xEF:
        tag = read_item(rdr, rdr.next_byte())
        fields = read_int(rdr)
        assert isinstance(tag, String)
        handler = read_handlers[tag.value]
        struct_cache.append(StructType(handler, tag, fields))
        return handler(rdr, tag, fields)

    if STRING_PACKED_LENGTH_START <= data <= (STRING_PACKED_LENGTH_START + 7):
        return String(internal_read_string(rdr, data - STRING_PACKED_LENGTH_START))

    if BYTES_PACKED_LENGTH_START <= data <= (BYTES_PACKED_LENGTH_START + 7):
        return String(internal_read_bytes(rdr, data - BYTES_PACKED_LENGTH_START))

    #if PRIORITY_CACHE_PACKED_START <= data <= (PRIORITY_CACHE_PACKED_START + 31):
    #    return priority_cache[data - PRIORITY_CACHE_PACKED_START]

    if STRUCT_CACHE_PACKED_START <= data <= (STRUCT_CACHE_PACKED_START + 15):
        handler = struct_cache[data - STRUCT_CACHE_PACKED_START]
        return handler.fn(rdr, handler.tag, handler.fields)

    if data == STRING:
        return String(internal_read_string(rdr, read_count(rdr)))


    assert False, hex(data)

def read_fressian_file(name):
    f = open(name, "rb")
    data = f.read()
    f.close()

    rdr = Reader(data)
    return read_item(rdr, rdr.next_byte())


def code_read_handler(rdr, tag, fields):
    name = read_item(rdr, rdr.next_byte())
    assert isinstance(name, String)

    args = read_item(rdr, rdr.next_byte())
    assert isinstance(args, Integer)

    bytecode = read_item(rdr, rdr.next_byte())
    assert isinstance(bytecode, String)

    vars = read_item(rdr, rdr.next_byte())
    assert isinstance(vars, Array)

    consts = read_item(rdr, rdr.next_byte())
    assert isinstance(consts, Array)

    stacksize = read_item(rdr, rdr.next_byte())
    assert isinstance(stacksize, Integer)

    locals = read_item(rdr, rdr.next_byte())
    assert isinstance(stacksize, Integer)

    return Code(name, args, bytecode.value, vars.data, consts.data, stacksize.to_int(), locals.to_int())

def var_read_handler(rdr, tag, fields):
    ns = read_item(rdr, rdr.next_byte())
    assert isinstance(ns, String)

    name = read_item(rdr, rdr.next_byte())
    assert isinstance(ns, String)

    return intern_var_from_str(ns.value, name.value)

def fn_read_handler(rdr, tag, fields):
    name = read_item(rdr, rdr.next_byte())
    assert isinstance(name, String)

    arities = read_item(rdr, rdr.next_byte())
    assert isinstance(arities, Array)
    arities_dict = {}
    for code in arities.data:
        assert isinstance(code, Code)
        arities_dict[code.arity.to_ruint()] = code


    return Fn(name, arities_dict)


class StructType(object):
    def __init__(self, fn, tag, fields):
        self.fn = fn
        self.tag = tag
        self.fields = fields

priority_cache = []
struct_cache = []
read_handlers = {"Code": code_read_handler,
                 "Var": var_read_handler,
                 "Fn" : fn_read_handler}