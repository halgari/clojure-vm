from clojure.vm.object import Object, extend, type_ids
from clojure.vm.primitives import nil


class Cons(object):
    type_id = type_ids.next()
    def __init__(self, *args):
        if len(args) == 2:
            self.head = args[0]
            self.tail = args[1]
            self.meta = nil

        if len(args) == 3:
            self.head = args[0]
            self.tail = args[1]
            self.meta = args[2]

        assert 1 < len(args) < 4, "Arity Exception"

