import inspect

class TypeIDTracker(object):
    def __init__(self):
        self.id = 0
    def next(self):
        self.id += 1
        return self.id

type_ids = TypeIDTracker()


class Object(object):

    def type(self):
        raise AbstractMethodException("Object.Type")


class VMException(Object):
    type_id = type_ids.next()
    def __init__(self, msg):
        self.msg = msg


class AbstractMethodException(Object):
    _typeid = type_ids.next()

    def __init__(self, msg):
        self.msg = msg

class Invokable(Object):
    def invoke(self, *args):
        raise AbstractMethodException("Invokable.invoke" + str(len(args)))

class PolymorphicFn(Invokable):
    def __init__(self, name, proto_var):
        self.name = name
        self.proto_var = proto_var
        self.type_table = {}
        self.rev = 0

    def extend(self, type_id, invokable):
        self.type_table[type_id] = invokable
        self.rev += 1

        self.proto_var.deref().extend_by(type_id)

    def invoke(self, *args):
        assert len(args) > 0
        tid = args[0].type()
        assert isinstance(tid, int)
        fn = self.type_table[tid]
        if not fn:
            raise AbstractMethodException("PolymorphicFn")

        return fn.invoke(*args)

class Protocol(Object):
    type_id = type_ids.next()
    def __init__(self, ns, name, fns):
        self.fns = fns
        self.ns = ns
        self.name = name

        self.implementors = {}

    def implemented_by(self, id):
        return id in self.implementors

    def extend_by(self, id):
        self.implementors[id] = id

class extend_protocol(object):
    def __init__(self, protocol):
        from clojure.vm.namespace import Var
        self.protocol = protocol
        if isinstance(self.protocol, Var):
            self.protocol = self.protocol.deref()

    def __call__(self, obj):
        self.protocol.extend_by(obj.type_id)
        return obj

WFn_tid = type_ids.next()

class AFn(Invokable):
    type_id = WFn_tid
    def type(self):
        return self.type_id








class extend(object):
    def __init__(self, *args):
        if len(args) == 2:
            self.fn = args[0]
            self.tp = args[1]
        if len(args) == 1:
            self.p_globals = inspect.currentframe().f_back.f_globals
            self.fn = None
            self.tp = args[0]

        assert 0 < len(args) < 3

    def __call__(self, invokable):
        from clojure.vm.namespace import Var
        argc = invokable.func_code.co_argcount
        type_id = self.tp.type_id
        tp = self.tp

        if self.fn is None:
            self.fn = self.p_globals[invokable.__name__]



        class WFn(AFn):
            def invoke(self, *args):
                assert len(args) == argc
                assert isinstance(args[0], tp)
                return invokable(*args)

        if isinstance(self.fn, Var):
            self.fn = self.fn.deref()



        assert isinstance(self.fn, PolymorphicFn)
        self.fn.extend(type_id, WFn())
        return self.fn


class Bool(Object):
    type_id = type_ids.next()
    def __init__(self, is_true):
        self.is_true = is_true

true = Bool(True)
false = Bool(False)
