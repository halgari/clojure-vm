from clojure.vm.object import Object, type_ids, VMException, true, false
from clojure.vm.string import String
from rpython.rlib.rarithmetic import r_uint

class Nil(Object):
    type_id = type_ids.next()

    def __init__(self):
        pass

    def type(self):
        return self.type_id

nil = Nil()


class Number(Object):
    pass


class Integer(Number):
    type_id = type_ids.next()
    def __init__(self, int_value):
        assert isinstance(int_value, int)
        self.int_value = int_value

    def type(self):
        return self.type_id

    def to_int(self):
        return self.int_value

    def to_ruint(self):
        return r_uint(self.int_value)

    def add(self, other):
        if isinstance(other, Integer):
            return Integer(self.int_value + other.int_value)

        assert False

    def sub(self, other):
        if isinstance(other, Integer):
            return Integer(self.int_value - other.int_value)

        raise VMException(String("Cannot add int to " + str(type(other))))

    def mul(self, other):
        if isinstance(other, Integer):
            return Integer(self.int_value * other.int_value)

        raise VMException(String("Cannot add int to " + str(type(other))))

    def div(self, other):
        if isinstance(other, Integer):
            return Integer(self.int_value / other.int_value)

        raise VMException(String("Cannot add int to " + str(type(other))))

    def eq(self, other):
        if isinstance(other, Integer):
            return true if self.int_value == other.int_value else false

        return false

    def gt(self, other):
        if isinstance(other, Integer):
            return true if self.int_value > other.int_value else false

        return false

    def lt(self, other):
        if isinstance(other, Integer):
            return true if self.int_value < other.int_value else false

        return false


