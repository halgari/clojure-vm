from clojure.vm.namespace import Var
from clojure.vm.bytecodes import *
from clojure.vm.object import *
from clojure.vm.primitives import nil, Number
from rpython.rlib.jit import JitDriver, hint, elidable, unroll_safe, promote
from rpython.rlib.rarithmetic import r_uint



def array_of_size(size):
    v = []
    for x in range(size):
        v.append(None)

    return v


def get_printable_location(ip, datasize, bytecodes, consts, vars):
    op = ord(bytecodes[ip])
    if op in HAS_ARG:
        return "%s %s" % (BYTECODES[op], str(ord(bytecodes[ip + 1])))
    else:
        return BYTECODES[op]

jitdriver = JitDriver(greens=['ip', 'datasize', 'bytecodes', 'consts', 'vars'], reds=['frame'], virtualizables=['frame'],
                      get_printable_location=get_printable_location)


def jitpolicy(driver):
    from rpython.jit.codewriter.policy import JitPolicy
    return JitPolicy()

class Arguments(object):
    _immutable_ = True
    def __init__(self, args, argc):
        self.args = args
        self.argc = argc

    @unroll_safe
    def unpack(self, frame):
        for x in range(self.argc):
            frame.data[frame.tos+x] = self.args[x]
        frame.tos += self.argc

class Frame(object):
    _virtualizable_ = ["data[*]"]

    def __init__(self, code):
        self = hint(self, access_directly=True, fresh_virtualizable=True)
        self.tos = r_uint(code.locals)
        self.data = [None] * (code.locals + code.stacksize)
        self.localsc = r_uint(code.locals)
        self.stacksize = r_uint(code.stacksize)

    def push(self, val):
        self.tos = promote(self.tos)
        depth = self.tos
        self.data[depth] = val
        self.tos = depth + 1

    def pop(self):
        assert self.tos >= self.localsc
        self.tos = promote(self.tos)
        depth = self.tos - 1
        val = self.data[depth]
        self.data[depth] = None
        self.tos = depth
        return val

    def set_local(self, idx, val):
        assert idx < self.localsc
        self.data[idx] = val

    def get_local(self, idx):
        return self.data[idx]

    #def get_arg(self, idx):
    #    assert idx < len(self.args)
    #    return self.args[idx]


def interpret(code, frame):
    assert isinstance(code, Code)
    bytecodes = code.bytecodes
    consts = code.consts
    vars = code.vars
    ip = 0
    datasize = code.stacksize + code.locals

    while True:
        jitdriver.jit_merge_point(ip=ip, datasize=datasize, bytecodes=bytecodes, consts=consts, vars=vars, frame=frame)

        op = ord(bytecodes[ip])
        #print get_printable_location(ip=ip, datasize=datasize, bytecodes=bytecodes, consts=consts, vars=vars)
        if op == POP:
            frame.pop()
            ip += 1
            continue

        elif op == PUSH_CONST:
            arg = ord(bytecodes[ip + 1])
            frame.push(consts[arg])
            ip += 2
            continue

        elif op == JMP:
            arg = ord(bytecodes[ip + 1])
            ip = arg
            jitdriver.can_enter_jit(ip=ip, datasize=datasize, bytecodes=bytecodes, vars=vars, consts=consts, frame=frame)
            continue


        elif op == COND_JMP:
            arg = ord(bytecodes[ip + 1])
            val = frame.pop()
            if val is not nil and val is not false:
                ip += 2
            else:
                ip = arg

            continue

        elif op == STORE_LOCAL:
            arg = ord(bytecodes[ip + 1])
            val = frame.pop()
            frame.set_local(arg, val)

            ip += 2

            continue

        elif op == NO_OP:
            ip += 1

            continue

        elif op == PUSH_LOCAL:
            arg = ord(bytecodes[ip + 1])

            frame.push(frame.get_local(arg))
            ip += 2

            continue

        elif op == PUSH_VAR:
            arg = ord(bytecodes[ip + 1])

            frame.push(vars[arg])
            ip += 2

            continue

        elif op == DEREF_VAR:
            var = frame.pop()
            assert isinstance(var, Var)

            frame.push(var.deref())
            ip += 1

            continue

        elif op == INIT_VAR:
            var = frame.pop()
            value = frame.pop()
            assert isinstance(var, Var)

            var.reset_root(value)

            ip += 1

            continue


        #elif op == PUSH_ARG:
        #    arg = r_uint(ord(bytecodes[ip + 1]))
        #    frame.push(frame.get_arg(arg))
        #    ip += 2
        #    continue

        elif op == EQ:
            b = frame.pop()
            a = frame.pop()
            assert isinstance(a, Number) and isinstance(b, Number)

            val = a.eq(b)
            frame.push(val)

            ip += 1

            continue

        elif op == LT:
            b = frame.pop()
            a = frame.pop()

            assert isinstance(a, Number) and isinstance(b, Number)

            val = a.lt(b)
            frame.push(val)

            ip += 1

            continue

        elif op == ADD:
            b = frame.pop()
            a = frame.pop()
            assert isinstance(a, Number) and isinstance(b, Number)

            val = a.add(b)

            frame.push(val)

            ip += 1

            continue

        elif op == INVOKE:
            argc = r_uint(ord(bytecodes[ip + 1]))
            argc = promote(argc)



            args = pop_values(frame, argc)
            fn = frame.pop()

            val = fn.call(args, argc)
            frame.push(val)
            ip += 2
            continue

        elif op == RETURN:
            return frame.pop()

        assert False, BYTECODES[op]


@unroll_safe
def pop_values(frame, argc):
    if argc == 0:
        return Arguments([], argc)
    elif argc == 1:
        return Arguments([frame.pop()], argc)
    elif argc == 2:
        b = frame.pop()
        a = frame.pop()
        return Arguments([a, b], argc)
    assert False
