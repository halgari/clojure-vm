from clojure.vm.object import Object, type_ids, Invokable, AFn, Protocol, PolymorphicFn
from clojure.vm.primitives import nil
from clojure.vm.symbol import Symbol, from_str as symbol_from_str
from rpython.rlib.jit import promote
import types, inspect


class Namespaces(object):
    def __init__(self):
        self.namespaces = {}

    def intern(self, name):
        assert isinstance(name, Symbol)
        assert name.ns == nil
        assert name.name

        if name.name in self.namespaces:
            return self.namespaces[name.name]

        ns = Namespace(name)
        self.namespaces[name.name] = ns

        return ns

namespaces = Namespaces()


class Namespace(Object):
    type_id = type_ids.next()

    def __init__(self, name):
        assert isinstance(name, Symbol)
        assert name.ns == nil
        self.name = name
        self.vars = {}

    def type(self):
        return self.type_id

    def intern_var(self, name):
        assert isinstance(name, Symbol)
        assert name.ns is nil, "Cannot give ns symbol to intern_var"

        if name.name.value in self.vars:
            return self.vars[name.name.value]

        var = Var(self, name)
        self.vars[name.name.value] = var

        return var


class Unbound(Object):
    type_id = type_ids.next()
    def type(self):
        return self.type_id

unbound = Unbound()

class Var(Invokable):
    type_id = type_ids.next()

    def __init__(self, ns, name):
        self.root = unbound
        self.ns = ns
        self.name = name

    def type(self):
        return self.type_id

    def reset_root(self, value):
        self.root = value

    def invoke(self, *args):
        assert isinstance(self.root, Invokable)
        return self.root.invoke(*args)

    def deref(self):
        return promote(self.root)



def intern_ns(name):
    assert isinstance(name, Symbol)
    return namespaces.intern(name)

def intern_from_str(name):
    return intern_ns(symbol_from_str(name))


def intern_var_from_str(ns_str, name_str):
    ns = intern_from_str(ns_str)
    name = symbol_from_str(name_str)

    return ns.intern_var(name)

class export_var(object):
    def __init__(self, ns_name, var_name):
        self.var = intern_var_from_str(ns_name, var_name)

    def __call__(self, obj):
        self.var.reset_root(obj)
        return obj


def wrap_fn(obj):
    if isinstance(obj, types.FunctionType):
        argc = obj.func_code.co_argcount
        if obj.func_code.co_flags & 0x04:
            func = obj
            class WFn(AFn):
                def invoke(self, *args):
                    assert isinstance(func, types.FunctionType)
                    assert len(args) >= argc
                    return func(*args)

            obj = WFn()
        else:

            func = obj
            class WFn(AFn):
                def invoke(self, *args):
                    assert len(args) == argc
                    return func(*args)

            obj = WFn()
    return obj

def export(obj):
    assert obj.__module__
    assert obj.__name__

    ns = obj.__module__
    name = demangle(obj.__name__)

    obj = wrap_fn(obj)
    var = intern_var_from_str(ns, name)
    var.reset_root(obj)

    return var


def demangle(str):
    str = str.replace("_", "-")
    str = str.replace("_QMARK_", "?")

    return str

def mangle(str):
    str = str.replace("-", "_")
    str = str.replace("?", "_QMARK_")

    return str


def defprotocol(proto_name, fns):
    p_globals = inspect.currentframe().f_back.f_globals
    ns = p_globals["__name__"]

    proto_var = intern_var_from_str(ns, proto_name)

    fn_vars = []
    for fn in fns:
        pfn = PolymorphicFn(fn, proto_var)
        var = intern_var_from_str(ns, fn)
        var.reset_root(pfn)
        fn_vars.append(var)
        p_globals[mangle(fn)] = var

    proto = Protocol(ns, proto_name, [fn_vars])

    proto_var.reset_root(proto)
    p_globals[mangle(proto_name)] = proto_var

    return proto_var


