(ns assembler.core
  (:require [clojure.data.fressian :as fress]
            [clojure.java.io :refer [copy]])
  (:import [java.io FileOutputStream]
           [org.fressian.handlers WriteHandler]))




(def  BYTECODES [
                 "NO_OP",
                 "POP",         ;; Pops TOS
                 "PUSH_CONST",  ;; Pushes the const at idx arg onto the stack
                 "PUSH_VAR",    ;; Pushes the var at the idx arg onto the stack
                 "DEREF_VAR",   ;; Pops a var from the stack and pushes the deref of that var
                 "INVOKE",      ;; fn is at TOS, args are below that
                 "APPLY",       ;; like invoke, but the last arg is considered to be a seq
                 "INIT_FIELD",  ;; sets the value of a field. can be mutable or imutable field
                 "GET_FIELD",   ;; gets the value of a field
                 "SET_FIELD",   ;; sets the value of a mutable field

                 "PUSH_LOCAL",
                 "STORE_LOCAL",
                 "PUSH_ARG",

                 "JMP",
                 "COND_JMP",

                 "EQ",
                 "NEQ",
                 "ADD",
                 "SUB",
                 "MUL",
                 "DIV",
                 "MOD",
                 "BIT_SHIFT_RIGHT",
                 "BIT_SHIFT_LEFT",
                 "BIT_AND",
                 "BIT_OR",
                 "BIT_XOR",
                 "BIT_NOT",
                 "BIT_COUNT",
                 "RETURN",


                 ])

(defmacro make-bytecodes []
  `(do
     ~@(for [idx (range (count BYTECODES))]
         `(def ^:const ~(symbol (nth BYTECODES idx)) ~idx))))

(make-bytecodes)


(defn fix-jumps [bytecode]
  (let [a (atom {})]
    (dotimes [n (count bytecode)]
      (let [v (nth bytecode n)]
        (when (and (keyword? v)
                   (not (namespace v)))
          (swap! a assoc (name v) n))))
    (map (fn [v]
           (cond
            (and (keyword? v) (not (namespace v))) NO_OP
            (and (keyword? v) (namespace v)) (get @a (name v))
            :else v))
         bytecode)))


(deftype Code [bytecode vars consts stacksize locals]
  clojure.lang.IDeref
  (deref [self]
    [bytecode vars consts stacksize locals]))

(deftype Var [ns name]
  clojure.lang.IDeref
  (deref [self]
    [ns name]))

(defn make-code [& {:as data}]
  (Code. (byte-array (map byte (fix-jumps (:bytecode data))))
         (into-array Object (:vars data))
         (into-array Object (:consts data))
         (:stacksize data)
         (:locals data)))

(def write-handlers
  (fress/associative-lookup {Code {"Code" (reify WriteHandler
                                            (write [_ w obj]
                                              (.writeTag w "Code" (count @obj))
                                              (doseq [itm @obj]
                                                (.writeObject w itm))))}}))




(let [add-fn (make-code :bytecode [ADD
                                   RETURN]
                        :vars []
                        :consts []
                        :locals 0
                        :stacksize 2)
      inner-code (make-code :bytecode [STORE_LOCAL 2
                                       PUSH_CONST, 0,
                                       STORE_LOCAL, 0,
                                       :loop-start
                                       PUSH_LOCAL, 0,
                                       PUSH_LOCAL, 2,
                                       EQ,
                                       COND_JMP, :to/loop-end
                                       PUSH_LOCAL 0
                                       PUSH_CONST 1
                                       PUSH_CONST 2
                                       INVOKE 2
                                       STORE_LOCAL, 0,
                                       JMP, :to/loop-start
                                       :loop-end
                                       PUSH_LOCAL, 0,
                                       RETURN]
                            :vars []
                            :consts [0 1 add-fn]
                            :stacksize 5
                            :locals 3)
      outer-code (make-code :bytecode [PUSH_CONST, 0,
                                       PUSH_CONST, 1,
                                       INVOKE, 1,
                                       RETURN]
                            :vars []
                            :consts [100000, inner-code]
                            :stacksize 2
                            :locals 0)

      arr (.array (fress/write outer-code
                               :handlers write-handlers))
      f (FileOutputStream. "/tmp/out.cljbc")]
  (.write f arr))
