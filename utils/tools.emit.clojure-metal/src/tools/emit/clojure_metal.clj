(ns tools.emit.clojure-metal
  (:refer-clojure :exclude [var var? compile macroexpand-1 emit])
  (:require [clojure.tools.analyzer :as an]
            [tools.emit.assembler :as asm]
            [tools.emit.macros :as macros])
  (:import [tools.emit.assembler Var]))



(defn debug [x]
  (clojure.pprint/pprint x)
  x)




(defn macroexpand-1 [form env]
  (if-let [sym (and (seq? form)
                    (symbol? (first form))
                    (not (namespace (first form)))
                    (first form))]
    (if-let [m (resolve (symbol "tools.emit.macros"
                                (str "-" (name sym))))]
      (apply m form form)
      form)
    form))

(defn parse [x env]
  (an/-parse x env))

(defn var? [v]
  (instance? Var v))

(defn create-var [nm {:keys [ns]}]
  (Var. (name ns) (name nm)))

;;;;; End Parsing ;;;;;;


;;;;; Begin Emit ;;;;;;

(def ^:dynamic vars)
(def ^:dynamic consts)
(def ^:dynamic locals)
(def ^:dynamic fn-arity 0)
(def ^:dynamic loop-start)

(defn add-var [var]
  (if-let [id (get @vars var)]
    id
    (get (swap! vars assoc var (count @vars))
         var)))

(defn add-local [nm]
  {:pre [nm]}
  (if-let [id (get @locals nm)]
    id
    (get (swap! locals assoc nm (count @locals))
         nm)))

(defn add-const [form]
  (if-let [id (get @consts form)]
    id
    (get (swap! consts assoc form (count @consts))
         form)))

(defmulti -emit :op)

(defmethod -emit :fn
  [{:keys [name methods env]}]
  (let [f (asm/make-fn (str (:ns env) "/" name)
                       (doall (map -emit methods)))]
    [asm/PUSH_CONST (add-const f)]))

(defn make-labels []
  (repeatedly #(keyword (gensym "label"))))

(defn to-jump-label [x]
  (keyword "to" (name x)))

(defmethod -emit :if
  [{:keys [test then else]}]
  (let [[then-label else-label end-label] (make-labels)]
    `[~@(-emit test)
      ~asm/COND_JMP ~(to-jump-label else-label)
      ~@(-emit then)
      ~asm/JMP ~(to-jump-label end-label)
      ~else-label
      ~@(-emit else)
      ~asm/JMP ~(to-jump-label end-label)
      ~end-label]))

(defn emit-store-params [params]
  (println "local --" (map :name params))
  (doall (mapcat (fn [{:keys [name]}]
                   (let [l (add-local name)]
                     (println l)
                     [asm/STORE_LOCAL (add-local name)]))
                 (reverse params))))

(defn mapping->vector [mp]
  (let [mp (zipmap (vals mp)
                   (keys mp))]
    (reduce
     (fn [acc idx]
       (conj acc (get mp idx)))
     []
     (range (count mp)))))


(defmethod -emit :fn-method
  [{:keys [fixed-arity body params] :as node}]
  (binding [vars (atom {})
            consts (atom {})
            locals (atom {})
            fn-arity fixed-arity]
    (let [bytecode (doall `[~@(emit-store-params params)
                     ~@(-emit body)
                     ~asm/RETURN])]
      (asm/make-code :bytecode bytecode
                     :vars (mapping->vector @vars)
                     :consts (mapping->vector @consts)
                     :locals (count @locals)
                     :arity fn-arity))))

(def host-bytecodes
  {'+ asm/ADD
   '< asm/LT
   '= asm/EQ})

(defmethod -emit :maybe-host-form
  [{:keys [class field] :as ast}]
  (if (= class 'clojure.vm.bytecode)
    (if (host-bytecodes field)
      `[~(host-bytecodes field)]
      (assert false))
    (assert false)))

(defmethod -emit :var
  [{:keys [var] :as ast}]
  (let [id (add-var var)]
    `[~asm/PUSH_VAR ~id
      ~asm/DEREF_VAR]))

(defmethod -emit :invoke
  [{:keys [fn args] :as node}]
  `[~@(when (not= (:op fn) :maybe-host-form) (-emit fn))
    ~@(mapcat -emit args)
    ~@(if (not= (:op fn) :maybe-host-form)
        [asm/INVOKE (count args)]
        (-emit fn))])

(defmethod -emit :const
  [{:keys [form]}]
  `[~asm/PUSH_CONST ~(add-const form)])

(defmethod -emit :local
  [{:keys [name]}]
  `[~asm/PUSH_LOCAL ~(add-local name)])

(defmethod -emit :recur
  [{:keys [exprs env] :as ast}]
  `[~@(mapcat -emit exprs)
    ~@(mapcat (fn [local]
                (println local "<<<<<<<<<<")
                [asm/STORE_LOCAL (add-local local)])
              (:loop-locals env))
    ~asm/JMP ~(to-jump-label loop-start)])

(defmethod -emit :loop
  [{:keys [bindings body]}]
  (let [[loop-start-label] (make-labels)]
    (binding [loop-start loop-start-label]
      (doall `[~@(mapcat -emit bindings)
               ~loop-start-label
               ~@(-emit body)]))))

(defmethod -emit :binding
  [{:keys [name init]}]
  `[~@(-emit init)
    ~asm/STORE_LOCAL ~(add-local name)])

(defmethod -emit :do
  [{:keys [statements ret]}]
  `[~@(mapcat (fn [ast]
               `[~@(-emit ast)
                 ~asm/POP])
              statements)
   ~@(-emit ret)])

(defmethod -emit :def
  [{:keys [name env init]}]
  (let [var-id (add-var (Var. (clojure.core/name (:ns env)) (clojure.core/name name)))]
    `[~@(-emit init)
      ~asm/PUSH_VAR ~var-id
      ~asm/INIT_VAR
      ~asm/PUSH_VAR ~var-id]))

(defn emit-code [form]
  (binding [vars (atom {})
            locals (atom {})
            consts (atom {})]
    (let [bytecode (doall `[
                     ~@(-emit form)
                     ~asm/RETURN])]
      (asm/make-code :bytecode bytecode
                     :vars (mapping->vector @vars)
                     :consts (mapping->vector @consts)
                     :arity 0
                     :locals (inc (count @locals))))))


(binding [an/parse parse
          an/macroexpand-1 macroexpand-1
          an/var? var?
          an/create-var create-var]
  (-> (an/analyze '(do
                     (defn + [a b]
                       (clojure.vm.bytecode/+ a b))
                     (defn < [a b]
                       (clojure.vm.bytecode/< a b))
                     (defn = [a b]
                       (clojure.vm.bytecode/= a b))
                     (loop [x 0]
                       (if (= x 100000)
                         x
                           (recur (+ x 1))
)))
                  (an/empty-env))
      (emit-code)
      debug
      (asm/write-code "/tmp/out3.cljbc")
      clojure.pprint/pprint))
