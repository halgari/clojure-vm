(ns tools.emit.assembler
  (:require [clojure.data.fressian :as fress]
            [clojure.java.io :refer [copy]])
  (:import [java.io FileOutputStream]
           [org.fressian.handlers WriteHandler]))


(def  BYTECODES [
                 "NO_OP",
                 "POP",         ;; Pops TOS
                 "PUSH_CONST",  ;; Pushes the const at idx arg onto the stack
                 "PUSH_VAR",    ;; Pushes the var at the idx arg onto the stack
                 "DEREF_VAR",   ;; Pops a var from the stack and pushes the deref of that var
                 "INIT_VAR",    ;;
                 "INVOKE",      ;; fn is at TOS, args are below that
                 "APPLY",       ;; like invoke, but the last arg is considered to be a seq
                 "INIT_FIELD",  ;; sets the value of a field. can be mutable or imutable field
                 "GET_FIELD",   ;; gets the value of a field
                 "SET_FIELD",   ;; sets the value of a mutable field

                 "PUSH_LOCAL",
                 "STORE_LOCAL",
                 "PUSH_ARG",

                 "JMP",
                 "COND_JMP",

                 "EQ",
                 "LT",
                 "GT",
                 "LTE",
                 "GTE",
                 "NEQ",
                 "ADD",
                 "SUB",
                 "MUL",
                 "DIV",
                 "MOD",
                 "BIT_SHIFT_RIGHT",
                 "BIT_SHIFT_LEFT",
                 "BIT_AND",
                 "BIT_OR",
                 "BIT_XOR",
                 "BIT_NOT",
                 "BIT_COUNT",
                 "RETURN",


                 ])

(defmacro make-bytecodes []
  `(do
     ~@(for [idx (range (count BYTECODES))]
         `(def ^:const ^:int ~(symbol (nth BYTECODES idx)) (int ~idx)))))

(make-bytecodes)

(def HAS-ARG #{PUSH_CONST
               PUSH_VAR
               INVOKE
               APPLY
               PUSH_LOCAL
               STORE_LOCAL
               JMP
               COND_JMP})

(defn stack-delta [op maybe-arg]
  (condp = (int op)
    NO_OP 0
    POP -1
    PUSH_CONST 1
    PUSH_VAR 1
    DEREF_VAR 0
    INIT_VAR -2
    INVOKE (- maybe-arg)
    PUSH_LOCAL 1
    STORE_LOCAL -1
    JMP 0
    COND_JMP 0
    ADD -1
    EQ -1
    GT -1
    LT -1
    GTE -1
    LTE -1
    RETURN -1
    :else (assert false (str (nth BYTECODES op)))))




(defn fix-jumps [bytecode]
  (let [a (atom {})]
    (dotimes [n (count bytecode)]
      (let [v (nth bytecode n)]
        (when (and (keyword? v)
                   (not (namespace v)))
          (swap! a assoc (name v) n))))
    (map (fn [v]
           (cond
            (and (keyword? v) (not (namespace v))) NO_OP
            (and (keyword? v) (namespace v)) (get @a (name v))
            :else v))
         bytecode)))


(deftype Code [name arity bytecode vars consts stacksize locals]
  clojure.lang.IDeref
  (deref [self]
    [name arity bytecode vars consts stacksize locals]))

(deftype Fn [name arities]
  clojure.lang.IDeref
  (deref [self]
    [name arities]))

(defn make-fn [name arities]
  (Fn. name arities))

(deftype Var [ns name]
  java.lang.Object
  (hashCode [self]
    (hash-combine (hash ns) (hash name)))
  (equals [self other]
    (and (instance? Var other)
         (= @self @other)))

  clojure.lang.IDeref
  (deref [self]
    [ns name]))

;; Really dumb, but should work
(defn calc-stack-size [bytecodes arity]
  (println "arity" arity)
  (loop [bcs (seq bytecodes)
         max-stack-size arity
         stack-size arity]
    (if bcs
      (let [op (first bcs)]
        (if (HAS-ARG (first bcs))
          (let [new-size (+ stack-size (stack-delta op (first (next bcs))))]
            (println new-size max-stack-size (nth BYTECODES (first bcs)))
            (recur (nnext bcs)
                   (max max-stack-size new-size)
                   new-size))
          (let [new-size (+ stack-size (stack-delta op nil))]
                        (println new-size max-stack-size (nth BYTECODES (first bcs)))
            (recur (next bcs)
                   (max max-stack-size new-size)
                   new-size))))
      max-stack-size)))


(defn make-code [& {:as data}]
  (let [bytecodes (fix-jumps (:bytecode data))
        stack-size (calc-stack-size bytecodes (:arity data))]
    (println bytecodes)
    (println stack-size "stack size")
    (Code.
     (or (:name data)
         (name (gensym "Code_")))
     (:arity data)
     (byte-array (map byte bytecodes))
     (into-array Object (:vars data))
     (into-array Object (:consts data))
     stack-size
     (:locals data))))


(def write-handlers
  (fress/associative-lookup {Code {"Code" (reify WriteHandler
                                            (write [_ w obj]
                                              (.writeTag w "Code" (count @obj))
                                              (doseq [itm @obj]
                                                (.writeObject w itm))))}
                             Var {"Var" (reify WriteHandler
                                          (write [_ w obj]
                                            (.writeTag w "Var" (count @obj))
                                            (doseq [itm @obj]
                                              (.writeObject w itm))))}
                             Fn {"Fn" (reify WriteHandler
                                        (write [_ w obj]
                                          (.writeTag w "Fn" 2)
                                          (let [[name arities] @obj]
                                            (.writeObject w name)
                                            (.writeObject w (into-array Object
                                                                        arities)))))}}))




(let [add-fn (make-code :bytecode [ADD
                                   RETURN]
                        :vars []
                        :consts []
                        :locals 0
                        :arity 2
                        :stacksize 2)
      inner-code (make-code :bytecode [STORE_LOCAL 2
                                       PUSH_CONST, 0,
                                       STORE_LOCAL, 0,
                                       :loop-start
                                       PUSH_LOCAL, 0,
                                       PUSH_LOCAL, 2,
                                       EQ,
                                       COND_JMP, :to/loop-end
                                       PUSH_LOCAL 0
                                       PUSH_CONST 1
                                       PUSH_VAR 0
                                       DEREF_VAR
                                       INVOKE 2
                                       STORE_LOCAL, 0,
                                       JMP, :to/loop-start
                                       :loop-end
                                       PUSH_LOCAL, 0,
                                       RETURN]
                            :vars [(Var. "user" "add-fn")]
                            :consts [0 1]
                            :stacksize 5
                            :arity 1
                            :locals 3)
      outer-code (make-code :bytecode [PUSH_CONST 2
                                       PUSH_VAR 0
                                       INIT_VAR
                                       PUSH_CONST, 0,
                                       PUSH_CONST, 1,
                                       INVOKE, 1,
                                       RETURN]
                            :vars [(Var. "user" "add-fn")]
                            :consts [100000, inner-code, add-fn]
                            :stacksize 2
                            :arity 0
                            :locals 0)

      arr (.array (fress/write outer-code
                               :handlers write-handlers))
      f (FileOutputStream. "/tmp/out.cljbc")]
  (.write f arr))


(defn write-code [code fname]
  (let [obj (fress/write code
                         :handlers write-handlers)
        arr (.array obj)
        f (FileOutputStream. fname)]
    (.write f arr)))
