(ns tools.emit.macros)


(defmacro -defn [nm & rest]
  `(def ~nm (fn* ~nm ~@rest)))


(defmacro -loop [& rest]
  `(loop* ~@rest))
