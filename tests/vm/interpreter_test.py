from clojure.vm.interpreter import Code, interpret, Frame
from clojure.vm.array import Array
from clojure.vm.primitives import Integer, nil
from clojure.vm.bytecodes import *
from clojure.vm.fressian_reader import read_fressian_file

def test_count_up():
    code = read_fressian_file("/tmp/out3.cljbc")
    assert isinstance(code, Code)
    assert interpret(code, Frame(code))




def target(driver, args):
    fns = {}
    for x in globals():
        if x.startswith("test_"):
            fns[x] = globals()[x]

    driver.exe_name = '__last__test'

    def entry_point(argv):
        for x in fns:
            print "Running ", x, "...",
            fns[x]()
            print "done"
        return 0

    return entry_point, None

class Dummy():
    pass

if __name__ == '__main__':
    entry, _ = target(Dummy, [])
    entry([])