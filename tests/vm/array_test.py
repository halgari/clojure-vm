from clojure.vm.array import Array
from clojure.vm.string import String, intern_from_str
from clojure.vm.primitives import nil


def test_create_array():
    assert Array([String("foo")])

def test_array_ops():
    a = Array([nil])

    a.aset(0, intern_from_str("foo"))

    assert a.aget(0) is intern_from_str("foo")

def target(driver, args):
    fns = {}
    for x in globals():
        if x.startswith("test_"):
            fns[x] = globals()[x]

    driver.exe_name = '__last__test'

    def entry_point(argv):
        for x in fns:
            print "Running ", x, "...",
            fns[x]()
            print "done"
        return 0

    return entry_point, None

class Dummy():
    pass

if __name__ == '__main__':
    entry, _ = target(Dummy, [])
    entry([])