from clojure.vm.namespace import intern_from_str, export


def test_namespace_from_str():
    assert intern_from_str("foo")

def test_namespaces_are_interned():
    assert intern_from_str("foo") is intern_from_str("foo")
    assert intern_from_str("bar") is not intern_from_str("foo")


def target(driver, args):
    fns = {}
    for x in globals():
        if x.startswith("test_"):
            fns[x] = globals()[x]

    driver.exe_name = '__last__test'

    def entry_point(argv):
        for x in fns:
            print "Running ", x, "...",
            fns[x]()
            print "done"
        return 0

    return entry_point, None

class Dummy():
    pass

if __name__ == '__main__':
    entry, _ = target(Dummy, [])
    entry([])