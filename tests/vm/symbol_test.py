from clojure.vm.symbol import from_str

def test_symbol_from_str():
    assert from_str("foo")
    assert from_str("foo", "bar")


def test_symbol_interns_strings():
    assert from_str("foo").name is from_str("foo").name
    assert from_str("foo", "bar").ns is from_str("foo", "bar").ns



def target(driver, args):
    fns = {}
    for x in globals():
        if x.startswith("test_"):
            fns[x] = globals()[x]

    driver.exe_name = '__last__test'

    def entry_point(argv):
        for x in fns:
            print "Running ", x, "...",
            fns[x]()
            print "done"
        return 0

    return entry_point, None

class Dummy():
    pass

if __name__ == '__main__':
    entry, _ = target(Dummy, [])
    entry([])