from clojure.vm.namespace import intern_var_from_str as intern
from clojure.vm.string import intern_from_str as intern_str

def test_vars_can_be_interned():
    assert intern("foo", "bar")
    assert intern("foo", "bar") is intern("foo", "bar")


def test_interned_vars_use_same_ns():
    assert intern("foo", "bar").ns is intern("foo", "bar").ns

def test_namespaces_contain_vars():
    v = intern("foo", "bar")
    assert "bar" in v.ns.vars


def target(driver, args):
    fns = {}
    for x in globals():
        if x.startswith("test_"):
            fns[x] = globals()[x]

    driver.exe_name = '__last__test'

    def entry_point(argv):
        for x in fns:
            print "Running ", x, "...",
            fns[x]()
            print "done"
        return 0

    return entry_point, None

class Dummy():
    pass

if __name__ == '__main__':
    entry, _ = target(Dummy, [])
    entry([])