import sys
import clojure
print(dir(clojure))
from clojure.vm.object import PolymorphicFn, Object, type_ids, extend
from clojure.vm.namespace import defprotocol

class Cons(Object):
    type_id = type_ids.next()
    def __init__(self, head, tail):
        self.head = head
        self.tail = tail

    def type(self):
        return self.type_id

class Nil(Object):
    type_id = type_ids.next()

    def type(self):
        return self.type_id

nil = Nil()

defprotocol("ISeq", ["first", "next", "conj"])

@extend(first, Cons)
def Cons_first(self):
    return self.head

@extend(next, Cons)
def Cons_next(self):
    return self.tail

@extend(conj, Nil)
def Nil_conj(self, other):
    return Cons(other, self)

@extend(conj, Cons)
def Cons_conj(self, other):
    return Cons(other, self)




def entry_point(argv):
    c = nil
    for x in range(int(argv[1])):
        c = conj.invoke(c, nil)

    count = 0
    while c is not nil:
        count += 1
        c = next.invoke(c)

    print "count", count
    return count


def target(*args):
    return entry_point, None

if __name__ == "__main__":
    entry_point(sys.argv)